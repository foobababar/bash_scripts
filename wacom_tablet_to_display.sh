devices=$( xsetwacom --list devices )
# split devices into 2 parts ? 
first_wacom_id=$(echo $devices | cut -d " " -f 8)
second_wacom_id=$(echo $devices | cut -d " " -f 18)

xsetwacom --set "${first_wacom_id}" mapToOutput eDP-1 && xsetwacom --set "${second_wacom_id}" mapToOutput eDP-1


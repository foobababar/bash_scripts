#!/bin/bash
DESTDIR=$1
NDIR=$2

if [ "${#}" -ne 2 ]; then 
		echo "wrong arg nb : expected 2 "
		exit 1
fi


if [ ! -d "$DESTDIR" ]; then
        echo "directory $DESTDIR doesn't exist, exiting"
		exit 1
fi

cd $DESTDIR

## get filenames into array
FILES=( $( ls | sort -V ) )

## number of files
N=${#FILES[@]}

## P = number of items per folders
P=$(( N / NDIR ))


## add remaining files to last folder
R=$(( N % NDIR ))


function f(){
	echo $(( $1 / $2 )) 
}

for i in $(seq 0 $((NDIR-1)) ); do
	mkdir "$i"
done

for i in $(seq 0 $((N-1)) ); do
	if [ "$i" -ge "$(( $N-$R ))" ]; then
		cp ${FILES[$i]} $((NDIR-1))
	else 
		cp ${FILES[$i]} $(f $i $P)
	fi
done


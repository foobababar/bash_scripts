#!/bin/bash

# move X files from SRCDIR to DESTDIR ; 
# if number of files < X on SRCDIR, move all files
# move files in order !

SRCDIR=$1
DESTDIR=$2
X=$3


if [ "${#}" -ne 3 ]; then
	echo "wrong number of args. Expected 3"
	exit 1
fi


if [ ! -d "$SRCDIR" ]; then
	echo "directory $SRCDIR doesn't exist, exiting" 
	exit 1
fi

if [ ! -d "$DESTDIR" ]; then
	echo "directory $DESTDIR doesn't exist, exiting" 
	exit 1
fi



cd $SRCDIR

FILES=( $( ls | sort -V | head -n "$X") )

for i in ${FILES[@]}; do
	mv "$i" "$DESTDIR"
done

echo "moved ${#FILES[@]} files"

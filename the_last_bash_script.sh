# BASH REFRESH

# LICENSE STUFF 
# public domain, use however you like






## ~~~~ HELLO WORLD ~~~~ ##
echo "hello world !"
printf "hello" "world" "!"






## ~~~~ VARIABLES ~~~~ ##
hw2="hello world 2 !"  
declare var="a variable"

echo "${hw2}"
echo "${var}"
# NOTE : set keyword is for shell variables !!!! 




## ~~~~ ARG SCRIPTS ~~~~ ##
echo "script name : $0"
echo "nb of provided args : ${#}"

[ ${#} -ne 0 ] && 
        echo "all script args : $@" && 
        echo "first parameter : $1"

#-------------------------------------------------------------------


if [ "${#}" -ne 2 ]; then
        echo "wrong number of args. Expected 2"
        exit 1
fi








## ~~~~ ARRAYS ~~~~ ##
declare -a a1=("foo" "bar" "foobar")
# or
a2=("doo" "far" "doofar")

echo "${a2[0]}" # array index
echo "${#a2[@]}" # array length

for i in "${a1[@]}"; do echo $i ; done





## ~~~~ COMPARISON OPERATORS ~~~~ ##
foo3=71
[ "${foo3}" -eq 71 ] && echo "foo3 is 71"
[[  "${foo3}" -ne 14 ]] && echo "foo3 is not 14"



## ~~~~ LOGIC OPERATORS ~~~~ ##
# || ; &&

# if dir exists, go on, else exit with error 
[ -d $DESTDIR ] || (echo "Directory : $DESTDIR doesn't exist." && exit 1)





## ~~~~ IF STATEMENT ~~~~ ##

foo2=0

if [ ${foo2} -eq 0 ] 
then
	echo "foo2 is 0"
else 
	echo "foo2 is not 0"
fi

test "${foo2}" -ne 1 && echo "foo2 is not 1"

#-------------------------------------------------------------------

var=2
if [ $var -eq 0 ]; then 
    echo "0"
elif [ $var -eq 1 ]; then
    echo "1"
else
    echo "neither 1 nor 0"
fi







## ~~~~ WHILE LOOP ~~~~ ##
iter=0
while [ ${iter} -lt 3 ] 
do
	echo $iter
	iter=$(( $iter + 1 ))
done








## ~~~~ FOR LOOP ~~~~ ##
files=$( ls )
for file in $files; do
        echo $file
done    

#-------------------------------------------------------------------

for i in {0..5}; do
        echo $i
done

#-------------------------------------------------------------------

for i in $(seq 0 1 10); do      #0 and 10 are included
        echo "$i"
done









## ~~~~ COUNTING ~~~~ ##
echo "8+2*(5-1)" | bc

foo0=$(( 1+7*3 ))
echo "${foo0}"








## ~~~~ STRING COMPARISON ~~~~##
# keywords: string comparison, str comparison
foo="rjkVJ>WMf6]tJZ{"
foo2="rjkVJ>WMf6]tJZ}"
foo3="vC5fP6!6{|A'\$h_"
foo4="rjkVJ>WMf6]tJZ{"

[[ $foo == $foo2 ]] && echo "unreachable" 
[[ $foo == $foo3 ]] && echo "unreachable" 
[[ $foo == $foo4 ]] && echo "OK" 
# with or without single quotes for varnames










## ~~~~ REGEX ~~~~~ ##
echo "SEND HELP"
# * = 0 or more times
# + = 1 or more times
# {n} : n times
# {n,} : at least n times
# {n,m} : between n and m times
# ^ : starts with
# $ : ends with
# [a-z][A-Z] : contains a letter (caps or not)
# [0-9] : contains number
# ! : invert meaning
# [[:digit:]] [[:alpha:]]
# | means or

# [^abc] : not containing any of abc ?
# +(...) : 1 or more
# ?(...) : 0 or one ?
# *(...) : 0 or more



# practice regex with ls : ls | egrep "..."
ls | egrep "[0-9]$"  # list files ending with a number
ls | egrep "^[0-9]"  # list files starting with a number
ls | egrep "(^[0-9])|(^f)" # list files starting with a number or with the letter f
ls | egrep "([0-9]){2}" # files containing 2 or more numbers
ls | egrep "([0-9]){2}$" # files ending with 2 or more numbers
ls | egrep "^[0-9]{2}$"  # filenames contening exactly 2 digits, and NOTHING MORE like 74 but not foo74
ls | egrep "[0-9]{2}"  # filenames contening exactly 2 numbers and anything else
ls | egrep "+([a-z])[0-9]{2}+([a-z])" # [1 or more letter(s)] [exactly 2 numbers] [1 or more letter(s)]  








## ~~~~ CUT ~~~~ ##
foo="foo;doo;bar"
echo "${foo}" | cut -d ";" -f 1  # foo
echo "${foo}" | cut -d ";" -f 2  # doo
echo "${foo}" | cut -d ";" -f 3  # bar
# NOTE : PIPE USED TO CHAIN COMMANDS







## ~~~~ TR ~~~~ ##
a3=($( echo "foo ; bar ; foobar " | tr -d ";" ))
# ^ IMPORTANT : transform into array with var=(...)

for i in "${a3[@]}"
do 
	echo $i
done












## ~~~~ FILE OPERATIONS ~~~~ ##
[ -d "mydir" ] && echo "mydir exists and is a directory" 
[ -f "file.txt" ] && echo "file exists" 
[ ! -f "nonexistent.txt" ] && echo "file doesn't exist"
# other options : -e -r -x -w








## ~~~ FUNCTIONS ~~~~ ##
function f1() {
    echo "function f1 called with ${#} arguments : "
    for i in "${@}"
    do
	    echo "${i}"
    done
    echo "________"
}

f1 "foo" "bar"

#-------------------------------------------------------------------

function f92() {
    echo 92 # use echo to return ! 
}

echo "${returned} * 2" | bc

#-------------------------------------------------------------------

function multiply() {
	echo "$(( $1*$2 ))"
}


returned2=$( multiply 7 8 )
echo $returned2

#-------------------------------------------------------------------











## ~~~~ OTHERS ~~~~ ##

# mv all .txt and .csv files into directory
mv *.{txt,csv} dirname

#-------------------------------------------------------------------

# exec
command="echo foo"
exec $command

#-------------------------------------------------------------------

# sort by names
ls | sort -V

#-------------------------------------------------------------------

# list 5 first files
ls | sort -V | head -n 5

#-------------------------------------------------------------------

# remove all directories from current dir : 
ls -d  */ | xargs rm -rf

#-------------------------------------------------------------------

#!/bin/bash 

[ ${#} -ne 2 ] && echo "script requires 2 arguments" && exit 1

s1=$(echo $1 | tr '[:upper:]' '[:lower:]')
s2=$(echo $2 | tr '[:upper:]' '[:lower:]')

echo $s1
echo $s2


if [[ "$s1" == "$s2" ]]; then 
	echo "ok"
else 
	echo "nope"
fi

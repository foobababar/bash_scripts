#!/bin/bash

DESTDIR=/var/www/html/joomla
USERNAME=joomla
PHPVERSION=3
JOOMLA_PACKAGE_NAME="Joomla_5.1.0-Stable-Full_Package.zip"

UBUNTUVERSION=$( awk -F= '$1 == "VERSION_ID" {gsub(/"/, "", $2); print $2}' /etc/os-release )

if [ "$UBUNTUVERSION" = "24.04" ]; then
	PHPVERSION=3
elif [ "$UBUNTUVERSION" = "22.04" ]; then
	PHPVERSION=1
else
	echo "wrong ubuntu version $UBUNTUVERSION"
	exit 1
fi
# install dependencies
apt install apache2 mariadb-server php8.$PHPVERSION libapache2-mod-php8.$PHPVERSION php8.$PHPVERSION-dev php8.$PHPVERSION-bcmath php8.$PHPVERSION-intl php8.$PHPVERSION-soap php8.$PHPVERSION-zip php8.$PHPVERSION-curl php8.$PHPVERSION-mbstring php8.$PHPVERSION-mysql php8.$PHPVERSION-gd php8.$PHPVERSION-xml unzip lynx vim -y




# download latest joomla

unzip  $JOOMLA_PACKAGE_NAME -d $DESTDIR

chown -R www-data:www-data $DESTDIR

chmod -R 755 $DESTDIR


echo "
<VirtualHost *:80>
	ServerAdmin webmaster@example.com
	
	ServerName joomla.example.com
	DocumentRoot $DESTDIR
	
	<Directory $DESTDIR>
	        Options FollowSymlinks
	        AllowOverride All
	        Require all granted
	</Directory>
	
	ErrorLog ${APACHE_LOG_DIR}/example.com_error.log
	CustomLog ${APACHE_LOG_DIR}/example.com_access.log combined
</VirtualHost>
" > /etc/apache2/sites-available/joomla.conf 


mysql --user=root -e "create database joomla; create user '$USERNAME'@'localhost' identified by 'password'; grant all on joomla.* to '$USERNAME'@'localhost'; flush privileges ;"


a2ensite joomla.conf

systemctl restart apache2

echo "now go to http://localhost/joomla/installation/index.php"

exit 0

